<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*Route::post('register', 'API\PassportController@register');

Route::group(['prefix' => 'v1'], function(){
	Route::resource('banks', 'TransactionController@getBankList');
});*/

Route::group(['middleware' => 'cors', 'prefix' => 'v1'], function(){
	Route::get('banks', 'TransactionController@getBankList');
	Route::get('transactions', 'TransactionController@getTransactions');
	Route::get('check-transaction/{id}', 'TransactionController@checkTransaction');
	Route::post('create-transaction', "TransactionController@createTransaction");
	Route::get('check-transaction-all', "TransactionController@checkTransactionAll");
});

