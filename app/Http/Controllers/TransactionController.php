<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use SoapClient;
use App\Http\Controllers\soapAuthController;
use Illuminate\Support\Facades\Cache;

use Illuminate\Support\Facades\Response;
use mobiledetect\Mobile_Detect;
use App\Transaction;
use DateTime;

class TransactionController extends BaseSoapController
{

	function __construct(){
		$this->url = "https://test.placetopay.com/soap/pse/?wsdl";
		$this->Login = "6dd490faf9cb87a9862245da41170ff2";
		$this->Trankey = "024h1IlD";	
		$this->Seed = date("c");	
		$this->paramsAuth = new soapAuthController();
	}

	public function getTransactions(){
		$transactions = Transaction::orderBy('id','DESC')->paginate(8);
		$response = Response::json($transactions, 200);
		return $response;
	}

    public function getBankList(){
    	date_default_timezone_set("America/Bogota");
		setlocale(LC_ALL,"es_ES");				
		try{		

			$banks = Cache::get('banks', function () {
			    self::setWsdl($this->url);
				$this->service = InstanceSoapClient::init();
				$auth_params = $this->paramsAuth->__Auth($this->Login, $this->Trankey, $this->Seed);
				$banks = $this->service->__soapCall("getBankList", $auth_params);

				$expiresAt = now()->addMinutes(1440);
				Cache::put('banks', $banks, $expiresAt);

				return $banks;
			});		

			$response = Response::json($banks, 200);

        	return $response;
		}catch(\Exception $e){
			return $e->getMessage();
		}	
    }   

    public function createTransaction(Request $request){
    	date_default_timezone_set("America/Bogota");
		setlocale(LC_ALL,"es_ES");				
		try{			
			self::setWsdl($this->url);
			$this->service = InstanceSoapClient::init();
			$auth_params = $this->paramsAuth->__Auth($this->Login, $this->Trankey, $this->Seed);			
			$auth_params[0]["transaction"] = array(
				"bankCode"=>$request->bankCode,
				"bankInterface"=>$request->bankInterface,
				"reference"=>md5(date("y-m-d h:i:s")),
				"description"=>$request->description,
				"language"=>"ES",
				"currency"=>"COP",
				"totalAmount"=>$request->totalAmount,
				"taxAmount"=>$request->taxAmount,
				"devolutionBase"=>$request->devolutionBase,
				"tipAmount"=>$request->tipAmount,
				"returnURL"=>$request->returnURL,
				"payer"=>array(
					'documentType' => $request->documentType,
				    'document' => $request->document,
				    'firstName' => $request->firstName,
				    'lastName' =>$request->lastName,
				    'company' =>$request->company,
				    'emailAddress' =>$request->emailAddress,
				    'address' =>$request->address,
				    'city' =>$request->city,
				    'province' =>$request->province,
				    'country' =>$request->country,
				    'phone' =>$request->phone,
				    'mobile' =>$request->mobile,
				),
				"ipAddress"=>$this->_getRealIP(),
			);

			$data = array();

			$transaction = $this->service->__soapCall("createTransaction", $auth_params);
			$success = false;
			if($transaction){
				$transaction = $transaction->createTransactionResult;
				$success = $transaction->returnCode; 
				$idTransaccion = Transaction::create([
					"transactionID"=>$transaction->transactionID,
			    	"sessionID"=>$transaction->sessionID,
			    	"returnCode"=>$transaction->returnCode,
			    	"trazabilityCode"=>$transaction->trazabilityCode,
			    	"transactionCycle"=>$transaction->transactionCycle,
			    	"bankCurrency"=>$transaction->bankCurrency,
			    	"bankFactor"=>$transaction->bankFactor,
			    	"bankURL"=>$transaction->bankURL,
			    	"responseCode"=>$transaction->responseCode,
			    	"responseReasonCode"=>$transaction->responseReasonCode,
			    	"responseReasonText"=>$transaction->responseReasonText,	
			    	"transactionState"=>\Config::get('constants.PENDING')								
				])->id;
				//Session::put('transaction', $idTransaccion);
				//\Session::set('transaction', $idTransaccion);
				session(['transaction' => $idTransaccion]);
				$message = "Transacción creada con éxito";
			}else{
				$message = "No se pudo crear la transacción por favor intente más tarde";
			}

			$data["message"] = $message;
			$data["success"] = $success;
			$data["transaction"] = $transaction;
			$response = Response::json($data, 200);
			return $response;		
			
		}catch(\Exception $e){
			return $e->getMessage();
		}
    }

    public function checkTransaction($id){
    	date_default_timezone_set("America/Bogota");
		setlocale(LC_ALL,"es_ES");				
		try{			
			self::setWsdl($this->url);
			$this->service = InstanceSoapClient::init();
			$auth_params = $this->paramsAuth->__Auth($this->Login, $this->Trankey, $this->Seed);
			$auth_params[0]["transactionID"] = $id;

			$transactionSoap = $this->service->__soapCall("getTransactionInformation", $auth_params);			
			if($transactionSoap->getTransactionInformationResult){
				if($transactionSoap->getTransactionInformationResult != "PENDING"){
					$transactionSql = Transaction::select("*")
					->where("transactionID", '=', $id)
					->update([
						"responseReasonText"=>$transactionSoap->getTransactionInformationResult->responseReasonText,
						"transactionState"=>\Config::get('constants.'.$transactionSoap->getTransactionInformationResult->transactionState),
					]);			
				}				
			}

			$transactionSql = Transaction::select("*")
			->where("transactionID", '=', $id)			
			->get();
			$transactionSql = $transactionSql ? $transactionSql[0] : false;
			$response = Response::json($transactionSql, 200);

        	return $response;
		}catch(\Exception $e){
			return $e->getMessage();
		}
    }

    public function checkTransactionAll(){
    	date_default_timezone_set("America/Bogota");
		setlocale(LC_ALL,"es_ES");	
		try{		

			$dataResponde = array();

			$date = new DateTime;
			$date->modify('-7 minutes');
			$formatted_date = $date->format('Y-m-d H:i:s');


			$transactions = Transaction::select("*")
			->where("transactionState", "=", "PENDING")
			->where('created_at', '<=', $formatted_date)
			->get();

			if(count($transactions) > 0){

				self::setWsdl($this->url);
				$this->service = InstanceSoapClient::init();
				$auth_params = $this->paramsAuth->__Auth($this->Login, $this->Trankey, $this->Seed);				

				$success = true;
				$itemsFails = array();

				foreach ($transactions as $key => $transaction){

					$auth_params[0]["transactionID"] = $transaction->transactionID;		

					$transactionSoap = $this->service->__soapCall("getTransactionInformation", $auth_params);			
					if($transactionSoap->getTransactionInformationResult){
						if($transactionSoap->getTransactionInformationResult != "PENDING"){
							$transactionSql = Transaction::select("*")
							->where("transactionID", '=', $transaction->transactionID)
							->update([
								"responseReasonText"=>$transactionSoap->getTransactionInformationResult->responseReasonText,
								"transactionState"=>\Config::get('constants.'.$transactionSoap->getTransactionInformationResult->transactionState),
							]);			
						}				
					}else{
						$itemsFails[] = $transaction->transactionID;
						$success = false;
					}

				}
				if(!$success){
					$dataResponde["message"] = "Algunas transacciones no respondieron a la solicitud correctamente";
					$dataResponde["success"] = false;
					$dataResponde["itemsFails"] = $itemsFails;
				}else{
					$dataResponde["message"] = "Transacciones actualizadas correctamente";
					$dataResponde["success"] = true;
				}				

			}else{
				$dataResponde["message"] = "No se encontraron transacciones pendientes por actualizar en este momento revisar más tarde si tiene alguna transacción pendiente por respuesta";
				$dataResponde["success"] = false;
			}

			$response = Response::json($dataResponde, 200);
			return $response;

		}catch(\Exception $e){
			return $e->getMessage();
		}
    }

    private function _getRealIP(){

        if (isset($_SERVER["HTTP_CLIENT_IP"])){

            return $_SERVER["HTTP_CLIENT_IP"];

        }elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){

            return $_SERVER["HTTP_X_FORWARDED_FOR"];

        }elseif (isset($_SERVER["HTTP_X_FORWARDED"])){

            return $_SERVER["HTTP_X_FORWARDED"];

        }elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){

            return $_SERVER["HTTP_FORWARDED_FOR"];

        }elseif (isset($_SERVER["HTTP_FORWARDED"])){

            return $_SERVER["HTTP_FORWARDED"];

        }else{

            return $_SERVER["REMOTE_ADDR"];

        }
    } 

}
