<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class soapAuthController extends Controller
{
    function __construct() { 
	    
  	} 

  	public function __Auth($login, $trankey, $seed){  		
		$hashString = sha1($seed.$trankey, false);
		$auth_params[] = array(
	  		"auth"=>array(
				"login"=>$login,
				"tranKey"=>$hashString,
				"seed"=>$seed,					
			)
	  	);
	  	return $auth_params;
  	}
}
