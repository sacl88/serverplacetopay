-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2018 at 06:01 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `placetopay`
--

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE IF NOT EXISTS `cache` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(256) NOT NULL,
  `value` text NOT NULL,
  `expiration` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transactionID` int(11) NOT NULL,
  `sessionID` varchar(32) NOT NULL,
  `returnCode` varchar(32) NOT NULL,
  `trazabilityCode` varchar(40) NOT NULL,
  `transactionCycle` int(11) NOT NULL,
  `bankCurrency` varchar(3) NOT NULL,
  `bankFactor` float NOT NULL,
  `bankURL` varchar(255) NOT NULL,
  `responseCode` int(11) NOT NULL,
  `responseReasonCode` varchar(3) NOT NULL,
  `responseReasonText` varchar(255) NOT NULL,
  `transactionState` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `transactionID`, `sessionID`, `returnCode`, `trazabilityCode`, `transactionCycle`, `bankCurrency`, `bankFactor`, `bankURL`, `responseCode`, `responseReasonCode`, `responseReasonText`, `transactionState`, `created_at`, `updated_at`) VALUES
(1, 1457789302, 'c9118467a3c54b66373103b246e63aa5', 'SUCCESS', '1420332', 2, 'COP', 1, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuxfGvkztZjtcuPYVCBIWK2v5y6cjDjxJpQycbiDYcQ0p', 3, '?-', 'Aprobada', 'OK', '2018-05-07 19:19:02', '2018-05-07 19:19:59'),
(2, 1457789442, '83912933d4af38e651e76597f9f92302', 'SUCCESS', '1420333', 2, 'COP', 1, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuxfGvkztZjtcuPYVCBIWK2uQbHPGyX5nSwnGhChPVhtK', 3, '?-', 'Tiempo excedido en pendiente', 'NOT_AUTHORIZED', '2018-05-07 19:25:10', '2018-05-07 19:54:16'),
(3, 1457792224, 'ed3dbf83adff472b44aab01f8b6fbca1', 'SUCCESS', '1420335', 6, 'COP', 1, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=V%2brHQIwzrDzFvDKfmDEJ7tNJHOdxBXujCeJRd2OqgQvfYTfJhnG4Ssu4vcln6l1y', 3, '?-', 'Fallida, Transacción no realizada en la entidad financiera', 'FAILED', '2018-05-07 21:26:19', '2018-05-07 22:12:04'),
(4, 1457793756, '31032828cd3bab4e5dc539b6e7ef2fbe', 'SUCCESS', '1420343', 6, 'COP', 1, 'https://registro.desarrollo.pse.com.co/PSEUserRegister/StartTransaction.htm?enc=tnPcJHMKlSnmRpHM8fAbuxfGvkztZjtcuPYVCBIWK2uB%2fe0cMpy1FyagrF9xeg2y', 3, '?-', 'Fallida, Transacción no realizada en la entidad financiera', 'FAILED', '2018-05-07 22:24:47', '2018-05-07 22:37:20');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
